% VDE Template for EUSAR Papers
% Provided by Barbara Lang und Siegmar Lampe
% University of Bremen, January 2002
% English version by Jens Fischer
% German Aerospace Center (DLR), December 2005
% Additional modifications by Matthias Wei{\ss}
% FGAN, January 2009

%-----------------------------------------------------------------------------
% Type of publication
\documentclass[a4paper,10pt]{article}
%-----------------------------------------------------------------------------
% Other packets: Most packets may be downloaded from www.dante.de and
% "tcilatex.tex" can be found at (December 2005):
% http://www.mackichan.com/techtalk/v30/UsingFloat.htm
% Not all packets are necessarily needed:
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
%\usepackage{ngerman} % in german language if required
\usepackage[nooneline,bf]{caption} % Figure descriptions from left margin
\usepackage{times}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[dvips]{graphicx}
\usepackage{epsfig}
\usepackage{url}
\input{tcilatex}
%-----------------------------------------------------------------------------
% Page Setup
\textheight24cm \textwidth17cm \columnsep6mm
\oddsidemargin-5mm                 % depending on print drivers!
\evensidemargin-5mm                % required margin size: 2cm
\headheight0cm \headsep0cm \topmargin0cm \parindent0cm
\pagestyle{empty}                  % delete footer and header
%----------------------------------------------------------------------------
% Environment definitions
\newenvironment*{mytitle}{\begin{LARGE}\bf}{\end{LARGE}\\}%
\newenvironment*{mysubtitle}{\bf}{\\[1.5ex]}%
\newenvironment*{myabstract}{\begin{Large}\bf}{\end{Large}\\[2.5ex]}%
%-----------------------------------------------------------------------------
% Using Pictures and tables:
% - Instead "table" write "tablehere" without parameters
% - Instead "figure" write "figurehere " without parameters
% - Please insert a blank line before and after \begin{figuerhere} ... \end{figurehere}
%
% CAUTION:   The first reference to a figure/table in the text should be formatted fat.
%
\makeatletter
\newenvironment{tablehere}{\def\@captype{table}}{}
\newenvironment{figurehere}{\def\@captype{figure}\vspace{2ex}}{\vspace{2ex}}
\makeatother



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% Please use capital letters in the beginning of important words as for example
\begin{mytitle}Barbeque - Binder\end{mytitle}
\begin{mysubtitle}A new RPC for the Barbeque project\end{mysubtitle}
%
% Please do not insert a line here
%
\\
Rogora Daniele\\
Matr. 771507, (daniele.rogora@mail.polimi.it)\\
\begin{flushright}
\emph{Report for the master course of Embedded Systems}\\
\emph{Reviser: PhD. Patrick Bellasi (bellasi@elet.polimi.it)}
\end{flushright}

Received: September, 2012\\
\hspace{10ex}

\begin{myabstract} Abstract \end{myabstract}
This project aims at extending the Barbeque project with a new RPC system that takes advantage of the Android Binder framework. A new plugin has been created as well as a new component for the RTLIB
that applies to the preexisting interface in order to make possible to switch flawlessly from the old fifo channel to the new one at compile time.

\vspace{4ex}	% Please do not remove or reduce this space here.
\begin{multicols}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

The BarbequeRTRM \cite{bbquecit} is a framework being developed at DEI - Politecnico di Milano - under the context of the European Project 2PARMA and it has been partially funded by the EC under the FP7-ICT-248716-2PARMA grant. This framework is the core of an highly modular and extensible run-time resource manager which provides support for an easy integration and management of multiple applications competing on the usage of one (or more) shared MIMD many-core computation devices. 
In order to complete its task it needs an RPC system, actually implemented with a FIFO queue working on files.
Recently the project has been ported to Android, that uses Binder as its standard method for IPC. In this paper it will be shown how Binder support has been integrated with Barbeque. 

%-----------------------------------------------------------------------------
\section{The Barbeque Open Source Project}
% Please avoid separations in titles
% and separate text manually

The main goal of the Barbeque project is to provide support for an easy integration and management of multiple applications competing on the usage of one (or more) shared MIMD many-core computation devices.
The framework design, which exposes different plugin interfaces, provides support for pluggable policies for both resource scheduling and the management of applications coordination and reconfiguration.
Applications integrated with this framework gets for-free a suitable instrumentation to support Design-Space-Exploration (DSE) techniques, which could be used to profile application behaviors to either optimize them at design time or support the identification of optimal QoS requirements goals as well as their run-time monitoring.

The main components of the framework are the bbque daemon and the RTLib. 
The former is the main server to which applications should register in order to use the capabilities of the project, while the latter is the library that must be used to integrate new applications with the framework.   
The library hides to the developer wanting to take advantage of the framework many technical details; one of these, and specifically the only one we are interested in in this paper, is the RPC system that connects the library on the application side to the Barbeque server.
We will focus on this in the next sections.

\subsection{The FIFO RPC}
Up to this moment there was only one RPC system supported by BBQUE, that is based on a FIFO queue working on specific files in the filesystem. 
%Thanks to the modularity of the whole project, the actual RPC implementation is well separated from the rest of the framework, thus being easy to modify without damaging other parts.DOVE????
Specifically the logic flow is represented in the picture \ref{fig:bbquefifofig}.

\begin{figurehere}
 \centering
 \includegraphics[width=8cm, height=4cm]{./eps/bbque-fifo.eps}
 \caption{Logical flow of the FIFO based RPC of Barbeque}
 \label{fig:bbquefifofig}
\end{figurehere}

All the data shared by the server and the client is encapsulated in messages. A message is a C struct that always begins with an header, containing the message type, a token used to match responses,
the pid of the app generating the message and a field containing the execution id. 
The header is followed by the payload, that depends on the message type.


\subsubsection{FIFO plugin}
In the Barbeque daemon there is an RPC proxy that calls the recv and send methods of the plugin, that is loaded at runtime. 
The recv method is synchronous; this means that it will block the execution of its thread until a new message is available.
The proxy actually performs different actions with the data returned by the plugin, such as creating a queue to give different priorities to the incoming messages depending on the type 
of the message. Then it offers the same recv and send methods to the core of the daemon.
When it invokes the send method of the loaded plugin, the proxy passes as arguments a pointer to a complete message to be sent. In fact the message is composed in the core module of the server.
When it invokes the recv method, instead, the argument passed is a pointer that will be allocated by the plugin and will contain a complete message received.

\subsubsection{RTLib}
On the other hand, on the RTLib side, the FIFO implementation is not a plugin, but is part of the library itself, contained in a RPC generic module, and is selected at compile time through an \#ifdef. 
This time there's not a proxy and there aren't recv and send requests either. Events are notified to the application directly from the code of the RPC upon data arrival from the server to another
part of the library.
This also means that sent messages are completely created in the FIFO module, and not passed as arguments, like in the server part.

\subsubsection{FIFO details}
The communication channel is composed of a pair of files in the filesystem; one represents the queue of the packets coming to the server, while the other is that of a single application running
with the RTLib. The standard read and write functions are used to access these files.
Since this communication channel is simply a sequential memory are that doesn't offer high level methods to retrieve informations about its content, every single message must be encapsulated in a FIFO level header.
This header, that is different from the one of the message itself, contains the size of the payload, its offset and the type of the message carried.


%-----------------------------------------------------------------------------
\section{Binder}
Android heavily depends on IPC for almost every feature it offers. As example an application that has to execute long operations in background as well as to provide a user interface needs to implement the background part in a service, that is a run in a different process with respect to the user interface, called activity.

Also broadcast events notifications, that is the notification of a system-wide event (like low battery), use this kind of communication, and many more examples could be done.

Thus, one of the new modules added to the linux kernel by Google is Binder, a lightweight remote procedure call mechanism.
It consists in three layers: a kernel module written in C, a C++ middleware and on top a set of Java API to expose it to the applications programmer. Fig. \ref{fig:middleware}.

Since the only level directly accessed in this project is that of the middleware, what follows will focus on that, but the same concepts are generally valid also for the Java level.

%---- AGGIUNGO ORIGIN????

\begin{figurehere}
 \centering
 \includegraphics[width=8cm, height=4cm]{./eps/middle.eps}
 \caption{The three layers of the Binder framework}
 \label{fig:middleware}
\end{figurehere}

\subsection{Usage of the C++ middleware}
Every Android application should be written in Java and run through the Dalvik VM. That is what Google says. But Google also released the NDK \cite{ndkurl}, the Native Development Kit, to write parts of the
application, tipically the cpu intensive ones, in C++ native code.
The point is that only few of the native libraries are available with the ndk, mainly those related to math, compression, OpenGL graphics, that could benefit from a native code.
As stated in the NDK web page \cite{ndkurl} ''native system libraries in the Android platform are not stable and may change in future platform versions. Your applications should only make use of the stable native system libraries provided in this NDK.''
Binder is one of these latter libraries. It should only be used in Java code, leaving the actual mapping to the underlying C++ middleware to the Java API Layer.
But the whole Barbeque project is written in C++, so the choice was obviously to stick with native code and interfacing directly with the C++ middleware. 
As a result some high level functionalities of Binder are lost, but still the methods exposed by the C++ interface are sufficient to build a working project.



\subsection{The communication model}
The communication model used by Binder is a client-server one. Basically the server registers itself to the system service manager, requesting the creation of a thread pool that will listen to incoming
transactions for the registered service. Note that this thread pool is created and managed by the OS, and it is entirely hidden to the programmer. Thus also the size and other parameters of the pool
are not set by the programmer.
Note that, as shown in figure \ref{fig:binderabstract}, both the client and the server are actually talking with the Binder kernel module, that manages all the transactions of the entire system.

\begin{figurehere}
 \centering
 \includegraphics[width=8cm, height=4cm]{./eps/commodel.eps}
 \caption{The communication model for the Binder framework}
 \label{fig:binderabstract}
\end{figurehere}

The first step to establish a connection is the creation of the thread pool by the server.
This happens with the help of the Android service manager. It is a special Binder with a known address; the server asks the service manager to publish a new service with a specific name and an object
that is the Binder implementation.

When a client wants to start a new communication with the server, it must get an handle to the server's Binder, that, in this case, is the service instantiated. 
For this purpose it queries the service manager providing the name of the requested service.
With the handle the client can access the proxy methods of the Binder object published by the server.

More details of the actual transaction will be shown in the next section.


\subsection{Transactions and Parcels}
The basic transaction is achieved with a call to the method 
\\{\bf boolean transact(int code, Parcel data, Parcel reply, int flags)}\\

The {\bf code} field is at programmer's disposal and is usually exploited to select the actual action to perform when the transaction is received.
The {\bf flags} field instead says if the transaction is one way or not; that is if the transact call should be synchronous or not on the caller side.
The typical usage of Binder is with a synchronous call to the transact method; for this project, instead, asynchronous calls were used, as discussed in the conclusions

The base type used for data exchange is the Parcel. It is a simple object that serializes data and exposes some useful methods to retrieve, as example, the size of the payload as well as the size
of the part of the payload still not read and so on.
One transaction always carries exactly one Parcel per way, that is one request and one reply. In fact also a reply Parcel is available, where the answer of the callee can be written and is returned to the caller.
When one transaction is requested, the callback method
\\{\bf boolean onTransact(int code, Parcel data, Parcel reply, int flags)}\\
on the callee side is activated, that obviously brings the same information that has been sent.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Integration}
Integrating Binder in the Barbeque project was quite easy but required some compromises.

\begin{figurehere}
 \centering
 \includegraphics[width=8cm, height=4cm]{./eps/integration.eps}
 \caption{The resulting commmunication model}
 \label{fig:binderbbque}
\end{figurehere}

The code tries to be as similar as possible to the FIFO one, so to make things clear and easy to understand thanks to a common flow.
One relevant diffrence, though, is that in this implementation, thanks to the high level methods offered by Parcel, there is no need for any header as in the FIFO case; now messages are sent without
any modification and without any added data, at least explicitly.

The code can be divided in three sets: one for the actual binder rpc plugin, one for the RTLib component and one for the common code shared by the first two parts.

\subsection{The common library}
This library implements the code needed by both the RTLib and Barbeque; specifically it contains the shared class that takes care of the communication through Binder.

Specifically this means our Binder implementation without the onTransact method; in fact this must accomplish different jobs in the RTLib and in the Barbeque, so the implementation has been moved to
different source files.

So the main method this library implements is 
\\{\bf void push(void *push\_data, size\_t size, uint32\_t code)}\\
This calls the transact method described above; {\bf push\_data} is the pointer to the message having size size, while {\bf code} in this case is used to expose the type of the message brought by the Parcel,
incremented by 1 in order to be compliant with the specifications of the transact method, that assert code should be greater than 0, while message type can be also 0.

The library also implements the helper function {\bf getBbqueServ} that retrieves the handle to the service thanks to the Android's service manager.

Also one reason for this common library was keeping an order in the source and avoid duplications of files. Indeed, since the Binder library shouldn't be used in native code, there were many
problems compiling and linking the project. One of the shortcomings is that some cpp source files from Android Open Source Project have to be compiled with the Barbeque;
moreover also some compiled libraries from AOSP are necessary to correctly link the binary code.

Since they are necessary for both the plugin and the RTLib, they have been moved to this common library, that is then statically linked to the other parts. 
As a result all the references to C++ source files, headers and libraries belonging to AOSP are confined in this library, and it becomes easier and quicker to handle the compilation and the distribution of the project.

It is also interesting to notice that this class doesn't have any reference to the type of data actually sent; it only manages generic pointers, without knowing their meaning. 


\subsection{The plugin}
This is the part used by the server. It consists of a plugin loaded at runtime. 
It respects the predefined interface of the rpc proxy: it provides recv and send methods, with the recv method blocking until a new message is effectively available.

The receiver channel initialization consists simply in a new service signaled to the system, as described above.

Since the reception of incoming data isn't requested by the main thread, but is notified by one thread belonging to the pool instantiated by Android, mutexes were needed to obtain the desired behaviour.
In this way a call to recv blocks until a signal is launched from the onTransact method, that notifies that the new message is ready for being used.

The onTransact method simply copies the data to a shared - with the main thread waiting on a recv - memory area and signals the conditional.
Note that currently the implementation assumes that in every moment there is always a thread waiting on the recv call. This is generally true, because this blocked thread belongs to the RPC proxy
and cycles through a very simple loop, and the onTransact are serialized using a mutex, so that from the thread pool only one at a time can execute.
But, in case this doesn't happen, there is a potential loss of some messages, since when the recv method is called it always assumes that there isn't any unread message and waits for a new one.
If necessary some checks can be added to overcome this problem, but inital tests have passed without errors of this kind.

At first a predefined queue was used to store incoming parcels until a recv consumed them; this was then removed because of the speed of the RPC proxy loop that made it unnecessary - at maximum 1
new parcel was present in the queue at a time -, and to avoid memory and cpu cycles waste.

The message is entirely copied to the Parcel data buffer without any modification; in fact all the information necessary to process it on the server side are available also in the Parcel structure (namely the size of the message itself).


\subsection{RTLib}
This is the part owned by the client side library. Since this module must create proper messages to be sent to the server based on information received, it has many similarities with the original FIFO one, especially in the code responsible for the creation of the messages. 

The initialization phase is just like that of the plugin: a new service is created and a reference to the Barbeque service is taken.

The onTransact method is different, though. It analyzes the message received and performs different actions based on the type of the message, just like the original FIFO implementation does;
this required some small changes to a few private methods that became public in order to be visible outside their own class.


%-----------------------------------------------------------------------------
\section{Conclusions}
Thanks to this project Barbeque has been extended to take advantage of the optimized Binder framework for the necessary RPC. This will be usable only on an Android device.

Although the goal of the project was reached, some drawbacks were found during the implementation.
 
The main disadvantage of the send-recv interface that the plugin has to implement, is that the common way that Binder should be used in is not respected. 
%In fact the one way flag is not even exposed in the Java interface, and is reserved for some operations like DEATH...
In fact the suggested way to use Binder would be to invoke the remote callback method synchronously; when the transact() method returns, the reply Parcel should contain valid data.
Unfortunately this doesn't fit well the plugin workflow. 
In fact, without further knowledge of the Barbeque internals, it's impossible to be sure that the next message to be sent will be for the sender of the last received parcel; moreover there may be messages sent not as a direct response of a previously received one.
So, to make things work, it would be necessary to add some memory buffers to store informations about waiting threads, some conditional statements to take the proper action depending on the flag field of the transaction.
Finally I've not found any public documentation about performance impact of two one way transactions with respect to one normal transaction, that should anyway demote to two different transactions at the kernel module level.
So, in conclusion, the usage of bidirectional transactions may be an interesting experiment to try to benchmark possible performance improvements.

The code of the project is publicly and freely available \cite{mygitrepo}

% We suggest the use of JabRef for editing your bibliography file (Report.bib)
\bibliographystyle{splncs}
\bibliography{Report}

\end{multicols}
\end{document}
